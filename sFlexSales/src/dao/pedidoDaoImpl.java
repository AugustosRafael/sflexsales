/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import model.pedido;
import view.mensagens;

/**
 *
 * @author rafaelaugusto
 */
public class pedidoDaoImpl implements pedidoDao{

    @Override
    public void cadastrar(ArrayList<pedido> listaP, String cpf, String data, String modo){
        mensagens me = new mensagens(null, true);
        Connection con = DBUtil.getInstance().getConnection();
        try {

            PreparedStatement stmt = con.prepareStatement("INSERT INTO pedido" +
                    " (cpf, hora, data, codigo, valor, quantidade, modP) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)" );

                for(int i = 0; i < listaP.size(); i++){
                    stmt.setString(1, cpf);
                    stmt.setString(2, listaP.get(i).getHora());
                    stmt.setString(3, data);
                    stmt.setInt(4,listaP.get(i).getCodigo());
                    stmt.setFloat(5, listaP.get(i).getValor());
                    stmt.setInt(6, listaP.get(i).getQuantidade());
                    stmt.setString(7, modo);
                    stmt.executeUpdate();
                }
                 
            me.setUndecorated(true);
            me.lblTitulo.setText("Atenção");
            me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconAten.png")));
            me.lblTexto.setText("Compra realizada com sucesso!");
            me.setVisible(true);
            
        } catch (SQLException ex) {
            me.setUndecorated(true);
            me.lblTitulo.setText("Erro");
            me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
            me.lblTexto.setText("Não realizado!");
            me.setVisible(true);
        }
    }

    @Override
    public void excluir() {
        
    }

   
    
}

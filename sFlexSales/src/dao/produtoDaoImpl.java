/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import model.produto;
import view.mensagens;

/**
 *
 * @author rafaelaugusto
 */
public class produtoDaoImpl implements produtoDao{

    @Override
    public void cadastrar(produto p) {
        mensagens me = new mensagens(null, true);
        
        Connection con = DBUtil.getInstance().getConnection();
        try {
            PreparedStatement stmt = con.prepareStatement("INSERT INTO produto" +
                    " (codigo, nome, quantidade, foto, valor, flag) " +
                    "VALUES (?, ?, ?, ?, ?,?)" );
            stmt.setInt(1, p.getCodigo());
            stmt.setString(2, p.getNome());
            stmt.setInt(3, p.getQuantidade());
            stmt.setBytes(4, p.getFoto());
            stmt.setFloat(5, p.getValor());
            stmt.setString(6, "1");
            stmt.executeUpdate();
            
            me.setUndecorated(true);
            me.lblTitulo.setText("Atenção");
            me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconAten.png")));
            me.lblTexto.setText("Cadastro realizado com sucesso!");
            me.setVisible(true);
        } catch (SQLException ex) {
                me.setUndecorated(true);
                me.lblTitulo.setText("Erro");
                me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
                me.lblTexto.setText("Produto já cadastrado!");
                me.setVisible(true);
        }
    }

    @Override
    public void excluir(int codigo, String nome) {
        mensagens me = new mensagens(null, true);
        Connection con = DBUtil.getInstance().getConnection();
        try {
            PreparedStatement stmt = con.prepareStatement("UPDATE produto "
                                                 + "set flag = ? WHERE codigo = ? AND nome = ? AND flag <> ?");
            stmt.setString(1, "0");
            stmt.setInt(2, codigo);
            stmt.setString(3, nome);
            stmt.setString(4, "0");
            int execute = stmt.executeUpdate();	
            
            if( execute == 1){
                me.setUndecorated(true);
                me.lblTitulo.setText("Atenção");
                me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconAten.png")));
                me.lblTexto.setText("Produto desabilitado!");
                me.setVisible(true);
            }else if(execute == 0){
                me.setUndecorated(true);
                me.lblTitulo.setText("Erro");
                me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
                me.lblTexto.setText("Alteração não concluida!");
                me.setVisible(true);
            }
            
        } catch (SQLException ex) {
        }
        
    }
    
    @Override
    public produto selectProduto(String nome, int validador){
        produto p = new produto();
        mensagens me = new mensagens(null, true);
        try {
            Connection con = DBUtil.getInstance().getConnection();
            String cmd = "";
            if(validador == 1){
                cmd = "SELECT * FROM produto WHERE nome like ? AND flag <> ?";
            }else if (validador == 0){
                cmd = "SELECT * FROM produto WHERE codigo = ? AND flag <> ?";
            }
            
            PreparedStatement stmt = con.prepareStatement( cmd );
            stmt.setString(1, nome );
            stmt.setString(2, "0" );
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) { 
                p.setCodigo(rs.getInt("codigo"));
                p.setNome(rs.getString("nome"));
                p.setQuantidade(rs.getInt("quantidade"));
                p.setFoto(rs.getBytes("foto"));
                p.setValor(rs.getFloat("valor"));
            }
	} catch (SQLException e){
	}
        
    return p;}

    @Override
    public ArrayList<produto> listar() {
    ArrayList<produto> lista = new ArrayList<>();
        try {
            Connection con = DBUtil.getInstance().getConnection();
            String cmd = "SELECT * FROM produto WHERE flag = ?";

            PreparedStatement stmt = con.prepareStatement( cmd );
            stmt.setString(1, "1" );
            ResultSet rs = stmt.executeQuery();
            
            //codigo, nome, quantidade, foto, valor, flag
                while(rs.next()){
                    produto p = new produto();
                    p.setCodigo(rs.getInt("codigo"));
                    p.setNome(rs.getString("nome"));
                    p.setQuantidade(rs.getInt("quantidade"));
                    p.setValor(rs.getFloat("valor"));
                    p.setFoto(rs.getBytes("foto"));
                    lista.add(p);
                }
                
        }catch(SQLException e){   
        }
    return lista; }
    
}

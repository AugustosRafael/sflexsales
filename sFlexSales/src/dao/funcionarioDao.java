/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import model.funcionario;

/**
 *
 * @author rafaelaugusto
 */
public interface funcionarioDao {
    void  cadastrar(funcionario f);
    void excluir(String cpf);
    int login(String senha, String cpf);
    ArrayList<funcionario> listar();
    
}

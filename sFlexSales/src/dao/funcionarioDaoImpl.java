/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import model.funcionario;
import view.mensagens;

/**
 *
 * @author rafaelaugusto
 */
public class funcionarioDaoImpl implements funcionarioDao{

    @Override
    public void cadastrar(funcionario f) {
        mensagens me = new mensagens(null, true);
        Connection con = DBUtil.getInstance().getConnection();
        try {
            
            PreparedStatement stmt = con.prepareStatement("INSERT INTO funcionario" +
                    " (cpf, rg, nome, logradouro, cep, numero, salario, horaInicial, horaFinal, senha, flag) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
            stmt.setString(1, f.getCpf());
            stmt.setString(2, f.getRg());
            stmt.setString(3, f.getNome());
            stmt.setString(4, f.getLogradouro());
            stmt.setString(5, f.getCep());
            stmt.setInt(6, f.getNumero());
            stmt.setFloat(7, f.getSalario());
            stmt.setString(8, f.getHoraInicial());
            stmt.setString(9, f.getHoraFinal());
            stmt.setInt(10, f.getSenha());
            stmt.setString(11, "1");
            stmt.executeUpdate();
            
            me.setUndecorated(true);
            me.lblTitulo.setText("Atenção");
            me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconAten.png")));
            me.lblTexto.setText("Cadastro realizado, sua senha: " + f.getSenha());
            me.setVisible(true);
            
        } catch (SQLException ex) {
            me.setUndecorated(true);
            me.lblTitulo.setText("Erro");
            me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
            me.lblTexto.setText("Funcionário já cadastrado!");
            me.setVisible(true);
        }
    }

    @Override
    public void excluir(String cpf) {
        mensagens me = new mensagens(null, true);
        Connection con = DBUtil.getInstance().getConnection();
        try {

            PreparedStatement stmt = con.prepareStatement("UPDATE funcionario "
                                                 + "set flag = ? WHERE cpf = ? AND flag <> ? AND flag <> ?");
            stmt.setString(1, "0");
            stmt.setString(2, cpf);
            stmt.setString(3, "3");
            stmt.setString(4, "0");
            int execute = stmt.executeUpdate();
            
            if( execute == 1){
                me.setUndecorated(true);
                me.lblTitulo.setText("Atenção");
                me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconAten.png")));
                me.lblTexto.setText("Funcionário desabilitado!");
                me.setVisible(true);
            }else if(execute == 0){
                me.setUndecorated(true);
                me.lblTitulo.setText("Erro");
                me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
                me.lblTexto.setText("Alteração não concluida!");
                me.setVisible(true);
            }
            
            
        }catch(SQLException e){
            e.getStackTrace();
        }
    }

    @Override
    public int login(String senha, String cpf) {
        int valido = 0;
        String l ="", s="", f ="";
        
        try {
            Connection con = DBUtil.getInstance().getConnection();
            String cmd = "SELECT cpf, senha, flag FROM funcionario WHERE cpf like ?";

            PreparedStatement stmt = con.prepareStatement( cmd );
            stmt.setString(1, cpf );
            ResultSet rs = stmt.executeQuery();
            
            if(rs.next()) { 
                 l = rs.getString("cpf");
                 s = rs.getString("senha");
                 f = rs.getString("flag");
            }else{
                
            }
            
            if(l.equals(cpf) & s.equals(senha)){
                switch (f) {
                    case "3":
                        valido = 2;
                        break;
                    case "1":
                        valido = 1;
                        break;
                    default:
                        valido = 0;
                        break;
                }
            }
                    
	}catch (SQLException e) {
            //e.printStackTrace();
	}
    return valido;}

    @Override
    public ArrayList<funcionario> listar() {
        ArrayList<funcionario> lista = new ArrayList<>();
        try {
            Connection con = DBUtil.getInstance().getConnection();
            String cmd = "SELECT * FROM funcionario WHERE flag = ?";

            PreparedStatement stmt = con.prepareStatement( cmd );
            stmt.setString(1, "1" );
            ResultSet rs = stmt.executeQuery();
            
                while(rs.next()){
                    funcionario f = new funcionario();
                    f.setCpf(rs.getString("cpf"));
                    f.setRg(rs.getString("rg"));
                    f.setNome(rs.getString("nome"));
                    f.setLogradouro(rs.getString("logradouro"));
                    f.setCep(rs.getString("cep"));
                    f.setNumero(rs.getInt("numero"));
                    f.setSalario(rs.getFloat("salario"));
                    f.setHoraInicial(rs.getString("horaInicial"));
                    f.setHoraFinal(rs.getString("horaFinal"));
                    lista.add(f);
                }
        }catch(SQLException e){
            
            
        }
    return lista;}

}

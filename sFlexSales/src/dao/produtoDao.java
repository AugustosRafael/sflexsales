/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import model.produto;

/**
 *
 * @author rafaelaugusto
 */
public interface produtoDao {
     void  cadastrar(produto p);
     void excluir(int codigo, String nome);
     produto selectProduto(String produto, int validador);
     ArrayList<produto> listar();

}

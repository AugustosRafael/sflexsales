/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.produtoDao;
import dao.produtoDaoImpl;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.produto;
import view.manterP;
import view.principalC;

/**
 *
 * @author rafaelaugusto
 */
public class produtoController {
    produtoDao pd = new produtoDaoImpl();
    
    public void salvar(manterP mp, byte[] imagem) {
        produto p = new produto();
        p.setCodigo(Integer.parseInt(mp.txtCodigo.getText()));
        p.setNome(mp.txtNome.getText());
        p.setQuantidade(Integer.parseInt(mp.txtQuantidade.getText()));
        p.setValor(Float.parseFloat(mp.txtValor.getText()));
        p.setFoto(imagem);
        pd.cadastrar(p);
    }
    
    private byte[] converter(File f, manterP p){
        boolean isPng = false;
        isPng = f.getName().endsWith("png");
        try {
            BufferedImage imagem = ImageIO.read(f);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int type = BufferedImage.TYPE_INT_RGB;
            
            if(isPng){
                type = BufferedImage.BITMASK;
            }
            
            BufferedImage nomeIm = new BufferedImage(p.lblFoto.getWidth() - 5, p.lblFoto.getHeight() - 5, type);
            Graphics2D g = nomeIm.createGraphics();
            g.setComposite(AlphaComposite.Src);
            g.drawImage(imagem, 0,0,p.lblFoto.getWidth() - 5, p.lblFoto.getHeight() - 5, null);
            
            if (isPng){
                ImageIO.write(nomeIm, "png", out);               
            }else{
                ImageIO.write(nomeIm, "jpg", out);                               
            }
            out.flush();
            byte[] vetor = out.toByteArray();
            out.close();
            return vetor;
        } catch (IOException ex) {
        }
    return null;}
    
    public void exluir(int codigo, String nome) {
        pd.excluir(codigo, nome);
    }
     
    public void cancelar() {
    }
    
    public void limpar() {
    }

    public byte[] escolherImagem(manterP p) {
        File f = null;
        JFileChooser jfc = new JFileChooser();
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Imagens em JPEG e PNG", "jpg","png");
        jfc.addChoosableFileFilter(filtro);
        jfc.setAcceptAllFileFilterUsed(false);
        if(jfc.showOpenDialog(p.lblFoto) == JFileChooser.APPROVE_OPTION){
            f = jfc.getSelectedFile();
            p.lblFoto.setText("");
            p.lblFoto.setIcon(new ImageIcon(f.toString()));
            p.lblFoto.setHorizontalAlignment(JLabel.CENTER);
        }
        byte[] imagem = null;
        
        if(f != null){
            imagem = converter(f, p);
        }
    return imagem;}
    
    public produto selectProduto(String nome, int validador){
        principalC pc = new principalC();
        produto p = new produto();       
        p = pd.selectProduto(nome,validador);   
    return p;}
    
    public ArrayList<produto> listar(){
    return pd.listar();}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.funcionarioDao;
import dao.funcionarioDaoImpl;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import model.funcionario;
import view.manterF;
import view.mensagens;

/**
 *
 * @author rafaelaugusto
 */
public class funcionarioController {
    funcionarioDao fd = new funcionarioDaoImpl();
    public void cadastrar(manterF f){
        funcionario fun = new funcionario();
        int senha = ( int ) ( 1000 + Math.random() * 9999);
        
        String suja = f.txtCpf.getText(), limpa = "";
        limpa += suja.substring(0,3); limpa += suja.substring(4,7); 
        limpa += suja.substring(8,11); limpa += suja.substring(12,14);
        fun.setCpf(limpa);
        
        limpa = "";
        suja = f.txtRg.getText();
        limpa += suja.substring(0,2); limpa += suja.substring(3,6); 
        limpa += suja.substring(7,10); limpa += suja.substring(11,12);
        fun.setRg(limpa);
        
        fun.setNome(f.txtNome.getText());
        fun.setLogradouro(f.txtLogradouro.getText());
        
        limpa = "";
        suja = f.txtCep.getText();
        limpa += suja.substring(0,5); limpa += suja.substring(6,9);
        fun.setCep(limpa);
        

        fun.setNumero(Integer.parseInt(f.txtNumero.getText()));

        fun.setSalario(Float.parseFloat(f.txtSalario.getText()));
       
        fun.setHoraInicial(f.txtHinicio.getText());
        fun.setHoraFinal(f.txtHfinal.getText());
        fun.setSenha(senha);
        fd.cadastrar(fun);
    }
    
    public void excluir(String cpf){
        fd.excluir(cpf);
    }
    
    
    public int login(String senha, String cpf){
        int valido;
        valido = fd.login(senha, cpf);
    return valido;}
    
    public ArrayList<funcionario> listar(){
    return fd.listar();}
    
}

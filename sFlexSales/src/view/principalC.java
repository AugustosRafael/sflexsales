/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.produtoController;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import model.pedido;
import model.produto;

/**
 *
 * @author rafaelaugusto
 */
public class principalC extends javax.swing.JFrame {
    int i = 0;
    mensagens me = new mensagens(this, true);
    ArrayList<String> itensPedido = new ArrayList<>();
    ArrayList<pedido> listaP = new ArrayList<>();
    String cpfF;
    produto p = new produto();
    /**
     * Creates new form principalC
     */
    public principalC() {
        initComponents();
        LocalDate data =java.time.LocalDate.now();
        DateTimeFormatter formatadorD= DateTimeFormatter.ofPattern("dd/MM/yyyy");
        txtData.setText(formatadorD.format(data));
    }
    
    public principalC(String cpf) {
        initComponents();
        LocalDate data =java.time.LocalDate.now();
        DateTimeFormatter formatadorD= DateTimeFormatter.ofPattern("dd/MM/yyyy");
        txtData.setText(formatadorD.format(data));
        cpfF = cpf;
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtQProduto = new javax.swing.JTextField();
        cbxTipo = new javax.swing.JComboBox<>();
        txtQuantidade = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        lblFoto = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstLista = new javax.swing.JList<>();
        txtValor = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnVoltar = new javax.swing.JButton();
        txtData = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnPagar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        txtTexto = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(254, 254, 254));
        setExtendedState(this.MAXIMIZED_BOTH);
        setUndecorated(true);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(254, 254, 254));

        txtQProduto.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        txtQProduto.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(38, 27, 115), null, new java.awt.Color(38, 27, 115)));
        txtQProduto.setCaretColor(new java.awt.Color(38, 27, 115));
        txtQProduto.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtQProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQProdutoActionPerformed(evt);
            }
        });
        txtQProduto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtQProdutoKeyPressed(evt);
            }
        });

        cbxTipo.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        cbxTipo.setForeground(new java.awt.Color(38, 27, 115));
        cbxTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome", "Código" }));

        txtQuantidade.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        txtQuantidade.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(38, 27, 115), null, new java.awt.Color(38, 27, 115)));
        txtQuantidade.setCaretColor(new java.awt.Color(38, 27, 115));
        txtQuantidade.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtQuantidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtQuantidadeKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(38, 27, 115));
        jLabel2.setText("Produto:");

        jPanel6.setBackground(new java.awt.Color(254, 254, 254));

        lstLista.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jScrollPane2.setViewportView(lstLista);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
            .addComponent(lblFoto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        txtValor.setFont(new java.awt.Font("Cantarell", 1, 24)); // NOI18N
        txtValor.setForeground(new java.awt.Color(38, 27, 115));
        txtValor.setText("0.0");

        jLabel6.setFont(new java.awt.Font("Cantarell", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(38, 27, 115));
        jLabel6.setText("Quantidade:");

        jLabel7.setFont(new java.awt.Font("Cantarell", 1, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(38, 27, 115));
        jLabel7.setText("R$");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtValor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtQProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbxTipo, 0, 208, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtQProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setBackground(new java.awt.Color(38, 27, 115));
        jPanel2.setForeground(new java.awt.Color(38, 27, 115));

        btnVoltar.setBackground(new java.awt.Color(236, 14, 14));
        btnVoltar.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        btnVoltar.setForeground(new java.awt.Color(254, 254, 254));
        btnVoltar.setText("X");
        btnVoltar.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(236, 14, 14), new java.awt.Color(236, 14, 14)));
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });

        txtData.setFont(new java.awt.Font("Cantarell", 1, 16)); // NOI18N
        txtData.setForeground(new java.awt.Color(251, 253, 254));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 558, Short.MAX_VALUE)
                .addComponent(btnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVoltar)
                    .addComponent(txtData))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setBackground(new java.awt.Color(254, 254, 254));

        jPanel5.setBackground(new java.awt.Color(254, 254, 254));

        btnPagar.setBackground(new java.awt.Color(38, 27, 115));
        btnPagar.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        btnPagar.setForeground(new java.awt.Color(254, 254, 254));
        btnPagar.setText("Pagar");
        btnPagar.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(38, 27, 115), new java.awt.Color(38, 27, 115)));
        btnPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagarActionPerformed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(38, 27, 115));
        btnCancelar.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(254, 254, 254));
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(38, 27, 115), new java.awt.Color(38, 27, 115)));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        txtTexto.setFont(new java.awt.Font("Cantarell", 1, 24)); // NOI18N
        txtTexto.setForeground(new java.awt.Color(38, 27, 115));
        txtTexto.setText("Total R$");

        txtTotal.setFont(new java.awt.Font("Cantarell", 1, 24)); // NOI18N
        txtTotal.setForeground(new java.awt.Color(38, 27, 115));
        txtTotal.setText("0");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnPagar, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(txtTexto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtTotal)
                .addContainerGap(156, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPagar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 12, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(jPanel3, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(707, 447));
    }// </editor-fold>//GEN-END:initComponents

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        loginView lv = new loginView();
        lv.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnVoltarActionPerformed

    private void btnPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagarActionPerformed
        if(!listaP.isEmpty()){
            String c = txtTotal.getText();
            c = c.replaceAll("," , ".");
            float conta = Float.parseFloat(c);
            pagamentoModo pm = new pagamentoModo();
            pm.setVisible(true);
            pm.pagarPedido(listaP, cpfF, txtData.getText(), conta); 
            limpar();
            lstLista.removeAll();
            listaP = new ArrayList<>();
            itensPedido = new ArrayList<>();
            txtTotal.setText("0.0");
        }else{
            me.setUndecorated(true);
            me.lblTitulo.setText("Erro");
            me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
            me.lblTexto.setText("Não há nenhum produto na lista!");
            me.setVisible(true);
        }
        
       
    }//GEN-LAST:event_btnPagarActionPerformed
    
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        me.setUndecorated(true);
        me.lblTitulo.setText("Atenção");
        me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconAten.png")));
        me.lblTexto.setText("Compra cancelada!");
        me.setVisible(true);    
        limpar();
        lstLista.removeAll();
        listaP = new ArrayList<>();
        itensPedido = new ArrayList<>();
        txtTotal.setText("0.0");
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtQProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQProdutoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQProdutoActionPerformed

    private void txtQuantidadeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuantidadeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) { 
            if(lblFoto.getIcon() != null && !txtValor.getText().equals("0.0")){
                String total;
                float f;
                pedido ped = new pedido();
                NumberFormat nf = new DecimalFormat ("##0.00", new DecimalFormatSymbols (new Locale ("pt", "BR")));   

                if(!txtQuantidade.getText().isEmpty()){
                        float valor = (float) p.getValor() * Integer.parseInt(txtQuantidade.getText());
                        itensPedido.add(" Produto: "+p.getNome() + "     Quantidade: "+ txtQuantidade.getText()+""
                        +"     Valor: " + nf.format(valor));

                        DefaultListModel listModel = new DefaultListModel();

                        for(String percorrer : itensPedido) {
                            listModel.addElement(percorrer);
                        }
                        lstLista.setModel(listModel);
                        total = (txtTotal.getText());
                        total = total.replaceAll(",", ".");
                        f = ((Float.parseFloat(total) + valor));
                        txtTotal.setText(nf.format(f));
                        
                        LocalTime hora =java.time.LocalTime.now();      
                        DateTimeFormatter formatadorT = DateTimeFormatter.ofPattern("HH:mm:ss");
                        
                        //lista de produtos para add em pedidosBD
                        ped.setCodigo(p.getCodigo());
                        ped.setCpf("");
                        ped.setHora(formatadorT.format(hora));
                        ped.setData(txtData.getText());
                        ped.setModoP("");
                        ped.setQuantidade(Integer.parseInt(txtQuantidade.getText()));
                        ped.setValor(p.getValor());
                        listaP.add(ped);
                        limpar();
                }else{
                    me.setUndecorated(true);
                    me.lblTitulo.setText("Erro");
                    me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
                    me.lblTexto.setText("Dados do pedido incompleto!");
                    me.setVisible(true);
                }
            }else{
                me.setUndecorated(true);
                me.lblTitulo.setText("Erro");
                me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
                me.lblTexto.setText("Dados do pedido incompleto!");
                me.setVisible(true);
            }
        }              
    }//GEN-LAST:event_txtQuantidadeKeyPressed

    private void txtQProdutoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQProdutoKeyPressed
        NumberFormat nf = new DecimalFormat ("#,##0.00", new DecimalFormatSymbols (new Locale ("pt", "BR")));   
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if(!txtQProduto.getText().isEmpty()){
                produtoController pc = new produtoController();
                if(cbxTipo.getSelectedItem().equals("Nome")){
                   p = pc.selectProduto(txtQProduto.getText(), 1);
                }else{
                   p = pc.selectProduto(txtQProduto.getText(), 0);
                }
                if(p.getFoto() != null){
                    ImageIcon imagem = new ImageIcon(p.getFoto());
                    imagem.setImage(imagem.getImage().getScaledInstance(150, 150, 100));
                    txtValor.setText(nf.format(p.getValor()));
                    lblFoto.setIcon(imagem);
                }else{
                    me.setUndecorated(true);
                    me.lblTitulo.setText("Erro");
                    me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
                    String obj;
                    if(cbxTipo.getSelectedItem().equals("Nome")){
                        obj = "Nome";
                    }else{
                        obj = "Código";
                    }
                    me.lblTexto.setText(obj + " do produto não existe!");
                    me.setVisible(true);
                }
            }else{
                me.setUndecorated(true);
                me.lblTitulo.setText("Erro");
                me.lblIcon.setIcon(new ImageIcon(getClass().getResource("/imagens/iconErro.png")));
                me.lblTexto.setText("Dados do pedido incompleto!");
                me.setVisible(true);
            }
        } 
    }//GEN-LAST:event_txtQProdutoKeyPressed
    public void limpar(){
        txtQProduto.setText("");
        txtQuantidade.setText("");
        txtValor.setText("0");
        lblFoto.setIcon(null);
        p = new produto();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(principalC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(principalC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(principalC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(principalC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new principalC().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnPagar;
    private javax.swing.JButton btnVoltar;
    public javax.swing.JComboBox<String> cbxTipo;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JLabel lblFoto;
    public javax.swing.JList<String> lstLista;
    public javax.swing.JLabel txtData;
    public javax.swing.JTextField txtQProduto;
    public javax.swing.JTextField txtQuantidade;
    private javax.swing.JLabel txtTexto;
    public javax.swing.JLabel txtTotal;
    public javax.swing.JLabel txtValor;
    // End of variables declaration//GEN-END:variables
}

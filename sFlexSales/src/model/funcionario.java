/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author rafaelaugusto
 */
public class funcionario {
    //não tem mais alteração nos tipos
    private String cpf;
    private String rg;    
    private String nome;
    private int senha;
    private String logradouro;
    private String cep;
    private int numero;
    private float salario;
    private String horaInicial;
    private String horaFinal;
    
    public funcionario(String cpf, String rg, String nome, String logradouro, String cep, int numero,
                       float salario, String horaInicial, String horaFinal, int senha) {
        this.cpf = cpf;
        this.rg = rg ;
        this.nome = nome;
        this.senha = senha;
        this.logradouro = logradouro;
        this.cep = cep;
        this.numero = numero;
        this.salario = salario;
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
    }

    public int getSenha() {
        return senha;
    }

    public void setSenha(int senha) {
        this.senha = senha;
    }

    public funcionario() { }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public String getHoraInicial() {
        return horaInicial;
    }

    public void setHoraInicial(String horaInicial) {
        this.horaInicial = horaInicial;
    }

    public String getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author rafaelaugusto
 */
public class pedido {
    //qPro = 
    private String cpf;
    private String hora;
    private String data;
    private int codigo;
    private float valor;
    private int quantidade;
    private String modoP;

    public pedido(String cpf, String hora, String data, int codigo, float valor, int quantidade, String modoP) {
        this.cpf = cpf;
        this.hora = hora;
        this.data = data;
        this.codigo = codigo;
        this.valor = valor;
        this.quantidade = quantidade;
        this.modoP = modoP;
    }

    public pedido() { }
    
    

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getModoP() {
        return modoP;
    }

    public void setModoP(String modoP) {
        this.modoP = modoP;
    }  
}
